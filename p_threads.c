#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
//#define NUM_HILOS	10 //cambiar cantidad de hilos crados



 void *hilos(void *t)
 {
    int i;
    long hiloID;


    hiloID = (long)t;
    printf("\n\n-| Hilo no. %ld iniciando...\n",hiloID);

    printf("\n\n->Hilo no. %ld finalizado. \n",hiloID);
    pthread_exit((void*) t);
 }

 int noHilos;

 void numeroH (){
    printf("\n          +------------------------------------------+");
    printf("\n          |        POSIX: HILOS EN LENGUAJE C        |");
    printf("\n          |                                           |");
    printf("\n          |           SISTEMAS OPERATIVOS            |");
    printf("\n          +------------------------------------------+");
    printf("\n\n\n>>>>> Cantidad de hilos a Ingresar . . . ");
    scanf ("%d",&noHilos);
 }

 int main (int argc, char *argv[])
 {
    numeroH();
    //pthread_t thread[NUM_HILOS];
    pthread_t thread[noHilos];
    pthread_attr_t at;
    int r;
    long t;
    void *st;


    pthread_attr_init(&at);
    pthread_attr_setdetachstate(&at, PTHREAD_CREATE_JOINABLE);

    for(t=0; t<noHilos /*t<NUM_HILOS*/; t++) {
       printf("\n\n|||POSIX::: creando hilo no. %ld\n", t);
       r = pthread_create(&thread[t], &at, hilos, (void *)t);
       if (r) {
          printf("\n> >ERROR;  %d\n", r);
          exit(-1);
          }
       }

    pthread_attr_destroy(&at);
    for(t=0; t<noHilos/*t<NUM_HILOS*/; t++) {
       r = pthread_join(thread[t], &st);
       if (r) {
          printf("\nERROR; %d\n", r);
          exit(-1);
          }
       printf("\n* * *POSIX::: proceso de hilo %ld completado....\n",t);
       }

 printf("\n\n\n>>>>>> POSIX::: Programa Terminado. Saliendo . . . <<<<<<<<\n");
 pthread_exit(NULL);
 }